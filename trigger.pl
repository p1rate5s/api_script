#!/usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Request::Common qw{ POST };
use LWP::Simple;
#use CGI;

# setup the parameters for the Jenkins pipeline to pass in
my $manifest = q( {  'manifest' : [
   {  'gav' : 'com.lrblackstock:best_app:1.1:zip', 'snapshot' : 'false', 'archive' : 'true' },
   {  'gav' : 'com.lrblackstock:test_app:1.40:zip', 'snapshot' : 'false', 'archive' : 'true' }
 ]
});
my $entry = 'deploy.sh';
my $target = '';
my $build_data = '';
my $token = 'rmurphy:88aa49fc1234a4bf14aa535e1a897231';
my $pipeline = 'jerry:8080/job/test_pipeline';
my $tjob = 'jerry:8080/job/helloworld/job/test_app2';
my $url = "http://$token\@$pipeline/buildWithParameters";
my %params = (
    'MANIFEST' => "$manifest",
    'ENTRY_POINT' => "$entry",
    'BUILD_DATA' => "$build_data",
    'TARGET_ENV' => "$target"
);
print %params;
print "\n\n";

# setup and execute the post to trigger the pipeline
my $ua      = LWP::UserAgent->new();
my $request = POST( $url, \%params );
my $content = $ua->request($request)->as_string();

print $content;

#grab the location url
$content =~ /Location: http:\/\/(\S+)/;
print $1;
print "\n\n";
my $location = $1;

#get the queue objects
my $job = get "http://$token\@$location/api/xml";
print $job;
print "\n\n";

#grab the pipeline id number
$job =~ /<number>(\d+)/;
print $1;
print "\n\n";
my $pipeline_id = $1;

#get the console output from the pipeline to get the jobid of the triggered jobid
#poll until the starting build shows up
my $console_output = get "http://$token\@$pipeline/$pipeline_id/consoleText";
do{
  sleep 1;
  $console_output = get "http://$token\@$pipeline/$pipeline_id/consoleText";
print ".";
}until $console_output =~ m/Starting building:/;
print $console_output;
print "\n\n";
$console_output =~ /Starting building: \w+...\w+ #(\d+)/;
print $1;
print "\n\n";
my $tjob_id = $1;

#finally get triggered job console $console_output
my $jconsole_output = get "http://$token\@$tjob/$tjob_id/consoleText";
do{
  sleep 1;
  $jconsole_output = get "http://$token\@$tjob/$tjob_id/consoleText";
}until $jconsole_output =~ m/Finished:/;

print $jconsole_output;
