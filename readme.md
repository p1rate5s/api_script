Steps to retrieve console output from pipeline with approval step:

1. Trigger pipeline
2. Grab pipeline queue ID
3. Grab pipeline job ID
4. Grab pipeline Console output and poll for "Starting building" which means it has been approved.
5. Grep for triggered job ID in the output
6. Grab job console output using jobid gotten above

- trigger.pl - works with opensource Jenkins without crumb
- trigger2.pl - works with Cloudbees Jenkins using the crumb
